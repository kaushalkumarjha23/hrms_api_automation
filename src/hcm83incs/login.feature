#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios 
#<> (placeholder)
#""
## (Comments)

#Sample Feature Definition Template
Feature: verify login
Scenario: admin login 

Given url 'http://algolabs.co.in/test/hrms_1.0.1.4/cms/BACKEND/webservices/login_user.php'
And request { user_email:"admin@83incs.com",user_password:"12345" }
When method POST
Then status 200
And match response ==  {"MAIN":{"USERS":[{"user_login_id":"1","user_id":"1","user_name":"admin@83incs.com","password":"12345","reset_pass":"7681ab1e091bef757763d5ebd546aab8","verify":"1","is_active":"1","created_by":"0","created_on":"2017-02-02 16:04:22","updated_by":"0","updated_on":"2017-02-02 15:04:22","SESSSION_INFO":[{"SESSION_ID":"1","SESSION_FIRSTNAME":"Admin","SESSION_FULL_NAME":"Admin 83incs"}]}],"SESSION_INFO":[{"SESSION_ID":"1","SESSION_FIRSTNAME":"Admin","SESSION_FULL_NAME":"Admin 83incs"}],"RESULT":{"STATUS":"SUCCESS","RESPONSE":"SUCC","API":"USER_LOGIN","USERID":"1","VALUE1":"0","VALUE2":"0","VALUE3":"0"}}} 